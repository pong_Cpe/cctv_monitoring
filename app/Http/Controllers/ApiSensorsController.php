<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SensorStore;

class ApiSensorsController extends Controller
{
    public function saveData($location,$hum,$tempc,$tempf){

        $tmpData['location'] = $location;
        $tmpData['hum'] = $hum;
        $tmpData['tempc'] = $tempc;
        $tmpData['tempf'] = $tempf;
        SensorStore::create($tmpData);

        echo "Save Complete\n";
    }

    public function getCurrent($location){
        $last = SensorStore::where('location',$location)->orderBy('created_at','desc')->first();
        return response()->json($last);
    }

    public function viewguage($location){
        $last = SensorStore::where('location', $location)->orderBy('created_at', 'desc')->first();
        return view('graph.guage', compact('last', 'location'));
    }

    public function getLast($location,$number)
    {
        $last = SensorStore::where('location', $location)->orderBy('created_at', 'desc')->paginate($number);
        return response()->json($last);
    }
}
