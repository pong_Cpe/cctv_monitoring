<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cctv;
use App\CctvGroup;
use Illuminate\Http\Request;

class CctvsController extends Controller
{
    public function __construct()
    {
       // $this->middleware('cors');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cctvs = Cctv::latest()->paginate($perPage);
        } else {
            $cctvs = Cctv::latest()->paginate($perPage);
        }

        return view('cctvs.index', compact('cctvs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $cctvgrouplist = CctvGroup::pluck('name', 'id');
        return view('cctvs.create',compact('cctvgrouplist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();

        $requestData['status'] = true;

        Cctv::create($requestData);

        return redirect('cctvs')->with('flash_message', ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cctv = Cctv::findOrFail($id);

        return view('cctvs.show', compact('cctv'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cctvgrouplist = CctvGroup::pluck('name', 'id');
        $cctv = Cctv::findOrFail($id);

        return view('cctvs.edit', compact('cctv', 'cctvgrouplist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $cctv = Cctv::findOrFail($id);

        $requestData['status'] = true;
        
        $cctv->update($requestData);

        return redirect('cctvs')->with('flash_message', ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Cctv::destroy($id);

        return redirect('cctvs')->with('flash_message', ' deleted!');
    }
}
