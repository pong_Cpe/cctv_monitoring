<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CctvGroup extends Model
{
    protected $fillable = [
        'name','desc','ip_in','ip_ext','domain','user','pass'
    ];
}
