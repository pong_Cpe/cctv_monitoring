<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cctv extends Model
{
    protected $fillable = [
        'name', 'cctv_group_id', 'ip', 'status', 'user', 'pass'
    ];

    public function nvr()
    {
        return $this->hasOne('App\CctvGroup', 'id', 'cctv_group_id');
    }
}
