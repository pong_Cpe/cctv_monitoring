<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorStore extends Model
{
    protected $fillable = [
        'location', 'hum', 'tempc', 'tempf'
    ];
}
