<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'CCTV Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $cctv->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('cctv_group_id') ? 'has-error' : ''}}">
    <label for="cctv_group_id" class="control-label">{{ 'NVR' }}</label>
    <select name="cctv_group_id" class="form-control" id="cctv_group_id" >
    @foreach ($cctvgrouplist as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($cctv->cctv_group_id) && $cctv->cctv_group_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('cctv_group_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ip') ? 'has-error' : ''}}">
    <label for="ip" class="control-label">{{ 'IP' }}</label>
    <input class="form-control" name="ip" type="text" id="ip" value="{{ $cctv->ip or ''}}" >
    {!! $errors->first('ip', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('user') ? 'has-error' : ''}}">
    <label for="user" class="control-label">{{ 'User' }}</label>
    <input class="form-control" name="user" type="text" id="user" value="{{ $cctv->user or ''}}" >
    {!! $errors->first('user', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pass') ? 'has-error' : ''}}">
    <label for="pass" class="control-label">{{ 'Pass' }}</label>
    <input class="form-control" name="pass" type="text" id="pass" value="{{ $cctv->pass or ''}}" >
    {!! $errors->first('pass', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
