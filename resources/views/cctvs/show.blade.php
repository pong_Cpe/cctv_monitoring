@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">cctv {{ $cctv->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/cctvs') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/cctvs/' . $cctv->id . '/edit') }}" title="Edit cctv"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('cctvs' . '/' . $cctv->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete cctv" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $cctv->id }}</td>
                                        <th>Pic</th><td>
                                        <img src="http://{{ $cctv->ip }}/ISAPI/Streaming/channels/201/picture" width="550px" alt="">
                                        @php
                                            $url ="http://".$cctv->ip."/ISAPI/Streaming/channels/201/picture";

$port="80";

$username=$cctv->user;

$password=$cctv->pass;

$fh = fopen("d:\\tst.jpg", "w") or die($php_errormsg);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");

curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);

curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_ALL);

curl_setopt($ch, CURLOPT_PORT, $port);

curl_setopt($ch, CURLOPT_TIMEOUT, 5);

curl_setopt($ch, CURLOPT_FILE, $fh);

 var_dump(Storage::disk('')->getAdapter()->getPathPrefix());

$success = curl_exec($ch);

  if ( !$success ) print "<br><B>Error!!</b><br>";

$output = curl_exec($ch);

$info = curl_getinfo($ch);

curl_exec($ch);

curl_close($ch);
                                        @endphp
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
