<html>
  <head>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
      google.charts.load('current', {'packages':['gauge']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Label', 'Value'],
          ['Humidity', 0],
          ['Temp', 0]
        ]);

        var options = {
          width: 700, height: 300,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 20
        };

        var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

        chart.draw(data, options);

        setInterval(function() {
          $.getJSON(
            '/cctv_check/apisensor/getcurrent/WemoSLoC1',
            function(mydata) {
              data.setValue(0, 1, mydata['hum']);
              data.setValue(1, 1,  mydata['tempc']);
              chart.draw(data, options);
            }
          );

          $.getJSON(
            '/cctv_check/apisensor/getLast/WemoSLoC1/10',
            function(mydata) {
              //console.log(mydata['data']);
              tmp = "";
              $.each(mydata['data'], function(key,value){
                tmp = tmp + " เวลา : " + value['created_at'];
                tmp = tmp + " | ความชื้น : " + value['hum'] + "%";
                tmp = tmp + " | อุณหภูมิ : " + value['tempc'] + " C<br/>";
              });
              $('#table_div').html(tmp);
            }
          );

          
        }, 5000);
      }
    </script>
  </head>
  <body>
  <h1>ระบบวัดค่า อุณหภูมิ และ ความชื้น ที่ <?php echo $location ?></h1>
    <div id="chart_div" style="width: 700px; height: 300px;"></div><br/>
    <div id="table_div"></div>
  </body>
</html>