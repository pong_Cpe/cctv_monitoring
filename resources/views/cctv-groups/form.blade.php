<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $cctvgroup->name or ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('desc') ? 'has-error' : ''}}">
    <label for="desc" class="control-label">{{ 'Description' }}</label>
    <input class="form-control" name="desc" type="text" id="desc" value="{{ $cctvgroup->desc or ''}}" >
    {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ip_in') ? 'has-error' : ''}}">
    <label for="ip_in" class="control-label">{{ 'Internal IP' }}</label>
    <input class="form-control" name="ip_in" type="text" id="ip_in" value="{{ $cctvgroup->ip_in or ''}}" >
    {!! $errors->first('ip_in', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ip_ext') ? 'has-error' : ''}}">
    <label for="ip_ext" class="control-label">{{ 'External IP' }}</label>
    <input class="form-control" name="ip_ext" type="text" id="ip_ext" value="{{ $cctvgroup->ip_ext or ''}}" >
    {!! $errors->first('ip_ext', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('domain') ? 'has-error' : ''}}">
    <label for="domain" class="control-label">{{ 'Domain url' }}</label>
    <input class="form-control" name="domain" type="text" id="domain" value="{{ $cctvgroup->domain or ''}}" >
    {!! $errors->first('domain', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('user') ? 'has-error' : ''}}">
    <label for="user" class="control-label">{{ 'CCTV NVR USer' }}</label>
    <input class="form-control" name="user" type="text" id="user" value="{{ $cctvgroup->user or ''}}" >
    {!! $errors->first('user', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pass') ? 'has-error' : ''}}">
    <label for="pass" class="control-label">{{ 'CCTV NVR Password' }}</label>
    <input class="form-control" name="pass" type="text" id="pass" value="{{ $cctvgroup->pass or ''}}" >
    {!! $errors->first('pass', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
