<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('cctv-groups', 'CctvGroupsController');
Route::resource('cctvs', 'CctvsController');
Route::get('/apisensor/save/{location}/{hum}/{tempc}/{tempf}', 'ApiSensorsController@saveData');
Route::get('/apisensor/getcurrent/{location}', 'ApiSensorsController@getCurrent');
Route::get('/apisensor/view/{location}', 'ApiSensorsController@viewguage');
Route::get('/apisensor/getLast/{location}/{number}', 'ApiSensorsController@getLast');

